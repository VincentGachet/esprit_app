class Player {
  final String id;
  final String name;
  final bool isGameMaster;
  List<int> cards;

  Player({this.id, this.name, this.isGameMaster, this.cards});

  factory Player.fromJson(Map<String, dynamic> json) {
    return Player(
        id: json['id'],
        name: json['name'],
        isGameMaster: json['isGameMaster'],
        cards: json['cards']);
  }
}
