class Room {
  final String id;
  final List<dynamic> players;
  final List<dynamic> cardPack;
  final String name;
  final bool isGameStarted;
  final int lives;
  final int shurikens;
  final int gameTurnNumber;

  Room(
      {this.id,
      this.players,
      this.cardPack,
      this.name,
      this.isGameStarted,
      this.lives,
      this.shurikens,
      this.gameTurnNumber});

  factory Room.fromJson(Map<String, dynamic> json) {
    return Room(
        id: json['id'],
        players: json['players'],
        cardPack: json['cardPack'],
        name: json['name'],
        isGameStarted: json['isGameStarted'],
        lives: json['lives'],
        shurikens: json['shurikens'],
        gameTurnNumber: json['gameTurnNumber']);
  }
}
