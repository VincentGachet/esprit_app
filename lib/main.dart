import 'package:snowy/ui/gameListPage.dart';
import 'package:flutter/material.dart';

//Sources : https://www.didierboelens.com/fr/2018/06/web-sockets---cr%C3%A9er-un-jeu-en-temps-r%C3%A9el/
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(buttonTheme: ButtonThemeData(height: 50, minWidth: 200)),
      home: GameListPage(),
    );
  }
}
