import 'dart:math';

import 'package:flutter/material.dart';

class PageContainer extends StatelessWidget {
  const PageContainer({this.body, this.onPop, this.icon});

  final Widget body;
  final Function onPop;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    Random random = new Random();
    return Scaffold(
        backgroundColor: Color.fromARGB(
            255, random.nextInt(255), random.nextInt(255), random.nextInt(255)),
        body: SafeArea(
            child: Column(children: <Widget>[
          backButton(context),
          Expanded(child: body)
        ])));
  }

  Widget backButton(context) {
    if (ModalRoute.of(context).canPop) {
      Random random = new Random();
      return GestureDetector(
          onTap: () {
            if (onPop != null) {
              onPop();
            } else {
              Navigator.of(context).pop();
            }
          },
          child: Container(
              alignment: Alignment.centerRight,
              width: MediaQuery.of(context).size.width,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white, shape: BoxShape.circle),
                  padding: const EdgeInsets.all(10),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Icon(icon != null ? icon : Icons.close))));
    } else {
      return Container();
    }
  }
}
