import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:snowy/constants.dart';
import 'package:snowy/data/models/Player.dart';
import 'package:snowy/networking/gameCommunication.dart';
import 'package:snowy/ui/PageContainer.dart';
import 'package:http/http.dart' as http;
import 'package:snowy/ui/gamePage.dart';
import 'package:snowy/ui/transitions/sizeRouteTransition.dart';

class PlayerPage extends StatefulWidget {
  const PlayerPage({this.roomId});

  final String roomId;

  @override
  State<StatefulWidget> createState() => _PlayerPageState(this.roomId);
}

class _PlayerPageState<PlayerPage> extends State {
  _PlayerPageState(this._roomId);

  TextEditingController _controllerPlayer = TextEditingController();
  TextEditingController _controllerRoom = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Random _random = new Random();
  Player _player;
  String _roomId;
  String _errorMessage;

  @override
  void initState() {
    super.initState();
    print("init state player page");
    game.init();
    game.listener = _onPlayerMessageReceived;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageContainer(
        body: Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    _roomId == null
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: TextFormField(
                              controller: _controllerRoom,
                              maxLength: 30,
                              showCursor: false,
                              textAlign: TextAlign.center,
                              autocorrect: false,
                              enableSuggestions: false,
                              decoration: InputDecoration(
                                  counterText: "",
                                  filled: true,
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(100),
                                    borderSide: BorderSide(
                                      width: 0,
                                      style: BorderStyle.none,
                                    ),
                                  ),
                                  hintText: 'Enter a room name'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Room name empty';
                                }
                                return null;
                              },
                            ))
                        : Container(),
                    Padding(
                        padding: const EdgeInsets.only(bottom: 40),
                        child: TextFormField(
                          controller: _controllerPlayer,
                          maxLength: 30,
                          showCursor: false,
                          textAlign: TextAlign.center,
                          autocorrect: false,
                          enableSuggestions: false,
                          decoration: InputDecoration(
                              counterText: "",
                              filled: true,
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(100),
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                              ),
                              hintText: 'Enter a player name'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Player name empty';
                            }
                            return null;
                          },
                        ))
                  ],
                ))),
        _errorMessage != null
            ? Text(_errorMessage,
                style:
                    TextStyle(backgroundColor: Colors.red, color: Colors.white))
            : Container(),
        FlatButton(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(100),
          ),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              if (_player == null) {
                if (_roomId != null) {
                  createPlayer(_roomId);
                } else {
                  createRoom();
                }
              }
            }
          },
          child: Text('PLAY'),
        )
      ],
    ));
  }

  createPlayer(roomId) {
    createPlayerRequest(_controllerPlayer.text, roomId).then((response) {
      dynamic responseJson = jsonDecode(response.body);
      if (responseJson['response'] != null &&
          responseJson['response']['message'] != null &&
          responseJson['response']['message'] == 'success') {
        if (responseJson['response']['data'] != null) {
          dynamic data = responseJson['response']['data'];
          _player = Player(
              id: data['id'],
              name: data['name'],
              isGameMaster: data['isGameMaster']);

          joinRoom(roomId);
        }
      } else if (responseJson['response'] != null &&
          responseJson['response']['message'] != null) {
        setState(() {
          _errorMessage = responseJson['response']['message'];
        });
      } else {
        setState(() {
          _errorMessage = "Error creating player";
        });
      }
    });
  }

  createRoom() {
    createRoomRequest(_controllerRoom.text).then((response) {
      dynamic responseJson = jsonDecode(response.body);
      if (responseJson['response'] != null &&
          responseJson['response']['message'] != null &&
          responseJson['response']['message'] == 'success') {
        if (responseJson['response']['data'] != null) {
          dynamic data = responseJson['response']['data'];
          String roomId = data['id'];
          createPlayer(roomId);
        }
      } else if (responseJson['response'] != null &&
          responseJson['response']['message'] != null) {
        print(responseJson['response']['message']);
      } else {
        print("Error creating room");
      }
    });
  }

  Future<http.Response> createPlayerRequest(String playerName, String roomId) {
    return http.post(
      "https://" + Constants.serverUrl + '/create/player',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{'name': playerName, 'roomId': roomId}),
    );
  }

  Future<http.Response> createRoomRequest(String roomName) {
    return http.post(
      "https://" + Constants.serverUrl + '/create/room',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': roomName,
      }),
    );
  }

  void joinRoom(String roomId) {
    _roomId = roomId;

    Map<String, dynamic> playerJoin = {
      'action': 'join',
      'data': {
        'player': {'id': _player.id},
        'room': {'id': roomId}
      }
    };
    game.send(jsonEncode(playerJoin));
  }

  _onPlayerMessageReceived(serverMessage) {
    try {
      dynamic response = jsonDecode(serverMessage);
      if (response != null) {
        String action = response['response']['action'];
        switch (action) {
          case 'player_entered':
            if (_roomId != null) {
              game.listener = null;
              Navigator.pushReplacement(
                  context, SizeRoute(page: GamePage(_player, _roomId)));
            }
            break;
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
