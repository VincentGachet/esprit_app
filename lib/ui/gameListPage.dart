import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:snowy/constants.dart';
import 'package:snowy/data/models/Room.dart';
import 'package:snowy/ui/PageContainer.dart';
import 'package:snowy/ui/playerPage.dart';
import 'package:snowy/ui/transitions/sizeRouteTransition.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class GameListPage extends StatefulWidget {
  GameListPage({Key key}) : super(key: key);

  @override
  _GameListPageState createState() => _GameListPageState();
}

class _GameListPageState extends State<GameListPage> {
  Future<List<Room>> futureRooms;
  static String host = 'https://' + Constants.serverUrl;

  Future<List<Room>> _fetchRooms() async {
    final response = await http.get(host + '/rooms');

    if (response.statusCode == 200) {
      dynamic roomsJson = json.decode(response.body);
      List<Room> rooms = [];
      for (dynamic roomJson in roomsJson) {
        rooms.add(Room.fromJson(roomJson));
      }
      return rooms;
    } else {
      throw Exception('Failed to load rooms');
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Random random = new Random();
    return FutureBuilder(
        future: _fetchRooms(),
        builder: (BuildContext context, AsyncSnapshot<List<Room>> snapshot) {
          if (snapshot.hasData) {
            List<Widget> roomList = [];
            for (Room room in snapshot.data) {
              roomList.add(GestureDetector(
                  onTap: () => Navigator.push(
                      context,
                      SizeRoute(
                          page: PlayerPage(
                        roomId: room.id,
                      ))),
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(100))),
                    child: Text(room.name +
                        ' (' +
                        room.players.length.toString() +
                        ')'),
                  )));
            }
            return PageContainer(
                body: Stack(children: <Widget>[
              RefreshIndicator(
                  onRefresh: () async {
                    setState(() {
                      _fetchRooms();
                    });
                  },
                  child: Column(
                    children: <Widget>[
                      roomList.isEmpty ? Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(100))),
                        margin: const EdgeInsets.all(20),
                        child: Text('Scroll to refresh rooms')
                      ) : Container(),
                      Expanded(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: ListView(children: roomList)
                        )
                      )
                    ],
                  )
          ),
              Positioned(
                  bottom: 20,
                  right: 20,
                  child: FloatingActionButton(
                    onPressed: () {
                      Navigator.push(context, SizeRoute(page: PlayerPage()));
                    },
                    child: Icon(Icons.add),
                    backgroundColor: Color.fromARGB(255, random.nextInt(255),
                        random.nextInt(255), random.nextInt(255)),
                  ))
            ]));
          } else if (snapshot.hasError) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  _fetchRooms();
                });
              },
              child: Scaffold(
                backgroundColor: Color.fromARGB(255, random.nextInt(255),
                    random.nextInt(255), random.nextInt(255)),
                body: Center(
                    child: Text('Connexion imposible, cliquez pour réessayer')),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
