import 'dart:convert';
import 'dart:math';

import 'package:snowy/data/models/Room.dart';
import 'package:snowy/networking/gameCommunication.dart';
import 'package:snowy/ui/pageContainer.dart';
import 'package:flutter/material.dart';
import 'package:snowy/data/models/Player.dart';

class GamePage extends StatefulWidget {
  const GamePage(this.player, this.roomId);

  final Player player;
  final String roomId;

  @override
  State<StatefulWidget> createState() =>
      _GamePageState(this.player, this.roomId);
}

class _GamePageState<GamePage> extends State {
  _GamePageState(this._player, this._roomId);

  Player _player;
  final String _roomId;
  String _playerId;
  Room _currentRoom;
  String _content = '';
  bool _isGamePlayable = false;
  int _playerNumber = 1;

  _onGameMessageReceived(serverMessage) {
    try {
      dynamic response = jsonDecode(serverMessage);
      if (response != null) {
        String message = response['response']['message'];
        String data = response['response']['data'];
        String action = response['response']['action'];
        switch (action) {
          case 'socket_connection_done':
          case 'go_home':
            game.listener = null;
            game.end();
            Navigator.pop(context);
            break;
          case 'player_entered':
            setRoom(data);
            setState(() {
              _content += '\n' + message;
            });
            if (_player.isGameMaster && _currentRoom.players.length > 1) {
              setState(() {
                _isGamePlayable = true;
              });
            }
            break;
          case 'player_resigned':
            setRoom(data);
            setState(() {
              _content += '\n' + message;
              _playerNumber = _currentRoom.players.length;
            });
            if (_player.isGameMaster && _currentRoom.isGameStarted) {
              setState(() {
                _isGamePlayable = false;
              });
            }
            break;
          case 'draw':
            setRoom(jsonDecode(data)['room']);
            setState(() {
              _content += '\n' + message;
            });
            break;
          case 'play_turn':
            int oldLivesNumber = _currentRoom.lives;
            setRoom(jsonDecode(data)['room']);

            setState(() {
              _content += '\n' + message;
              if (_currentRoom.lives < oldLivesNumber) {
                _content += '\n' + "Life loss...";
              } else {
                _content += '\n' + "All good !";
              }

              if (_currentRoom.lives == 0) {
                _content += '\n' + "Game ended";
              }
            });
            break;
          default:
            setState(() {
              _content += '\n' + message;
            });
            break;
        }
      }
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 24.0),
        child: Text(_content),
      );
    } catch (e) {
      print(e.toString());
      return Text(e.toString());
    }
  }

  send(dynamic data) {
    game.send(json.encode(data));
  }

  @override
  void initState() {
    super.initState();
    _playerId = _player.id;
    game.listener = _onGameMessageReceived;
  }

  @override
  Widget build(BuildContext context) {
    if (_currentRoom != null) {
      _playerNumber = _currentRoom.players.length;
    }
    return PageContainer(
        onPop: _onPop,
        body: Container(
            padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100))),
                      child: Text(_playerNumber < 2
                          ? 'Waiting for other players...'
                          : 'Player number : ' +
                              _playerNumber.toString() +
                              '/4')),
                  Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100))),
                      child: Text(_currentRoom != null
                          ? 'Turn : ' + _currentRoom.gameTurnNumber.toString()
                          : 'Waiting for the game to start...')),
                  Container(
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(100))),
                      child: livesWidget()),
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          margin: const EdgeInsets.only(top: 5, bottom: 10),
                          width: MediaQuery.of(context).size.width,
                          constraints: new BoxConstraints(minHeight: 40),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Scrollbar(
                              child: SingleChildScrollView(
                                  child: Text(_content))))),
                  _isGamePlayable && !_currentRoom.isGameStarted
                      ? FlatButton(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(100),
                          ),
                          onPressed: () {
                            dynamic startGameAction = {
                              'action': 'start_game',
                              'data': {
                                'room': {'id': _roomId}
                              }
                            };
                            send(startGameAction);
                          },
                          child: Text('PLAY'),
                        )
                      : Container(),
                  Scrollbar(
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: cardsWidget()))
                ])));
  }

  void _onPop() {
    dynamic playerResign = {
      'action': 'resign',
      'data': {
        'player': {'id': _player.id},
        'room': {'id': _roomId}
      }
    };
    send(playerResign);
  }

  void setRoom(String data) {
    dynamic roomsJson = json.decode(data);
    List<Room> rooms = [];
    for (dynamic roomJson in roomsJson) {
      rooms.add(Room.fromJson(roomJson));
    }
    _currentRoom = rooms.firstWhere((room) => room.id == _roomId, orElse: null);
    dynamic playerFromRoom = _currentRoom.players.firstWhere((player) => player['id'] == _playerId);
    _player = Player(
      id: playerFromRoom['id'],
      name: playerFromRoom['name'],
      isGameMaster: playerFromRoom['isGameMaster'],
      cards: List<int>.from(playerFromRoom['cards'])
    );
  }

  Widget cardsWidget() {
    List<Widget> cardsButtons = [];
    if (_player.cards != null) {
      for (int card in _player.cards) {
        cardsButtons.add(GestureDetector(
            onTap: () {
              dynamic playCardAction = {
                'action': 'play_card',
                'data': {
                  'player': {'id': _player.id},
                  'room': {'id': _roomId},
                  'card': card
                }
              };
              send(playCardAction);
              _player.cards.remove(card);
              setState(() {});
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                margin: const EdgeInsets.symmetric(horizontal: 10),
                width: MediaQuery.of(context).size.width * 0.25,
                height: 40,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                ),
                child: Text(card.toString(),
                    style: TextStyle(fontWeight: FontWeight.bold)))));
      }
    }
    return Row(mainAxisSize: MainAxisSize.max, children: cardsButtons);
  }

  Row livesWidget() {
    List<Icon> livesIconsList = [];
    if (_currentRoom != null && _currentRoom.lives > 0) {
      for (int i = 0; i < _currentRoom.lives; i++) {
        livesIconsList.add(Icon(
          Icons.favorite,
          color: Colors.pink,
          size: 22,
          semanticLabel: 'Heart',
        ));
      }
    }
    return Row(mainAxisSize: MainAxisSize.min, children: livesIconsList);
  }
}
