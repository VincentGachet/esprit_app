import 'package:flutter/foundation.dart';
import 'package:snowy/constants.dart';
import 'package:web_socket_channel/io.dart';

SocketManager sockets = SocketManager();

class SocketManager {
  static final SocketManager _instance = SocketManager._internal();

  factory SocketManager() {
    return _instance;
  }

  SocketManager._internal();

  bool _isOn = false;

  IOWebSocketChannel _channel;

  ObserverList<Function> _listeners = ObserverList<Function>();

  initCommunication() async {
    await reset();
    try {
      _channel = IOWebSocketChannel.connect("wss://" + Constants.serverUrl);
      _channel.stream.listen(_onReceptionOfMessageFromServer, onError: (e) {
        print('connection error : ' + e);
      }, onDone: () {
        print('connection done');
        _isOn = false;
        _listeners.forEach((Function callback) {
          callback('{"response": {"action":"socket_connection_done"}}');
        });
      }, cancelOnError: false);
    } catch (e) {
      print("INIT SOCKET ERROR : " + e);
    }
  }

  reset() {
    if (_channel != null) {
      if (_channel.sink != null) {
        _listeners = ObserverList<Function>();
        _channel.sink.close();
        _isOn = false;
      }
    }
  }

  addListener(Function callback) {
    _listeners.add(callback);
  }

  removeListener(Function callback) {
    _listeners.remove(callback);
  }

  send(String message) {
    if (_channel != null) {
      if (_channel.sink != null && _isOn) {
        _channel.sink.add(message);
      }
    }
  }

  _onReceptionOfMessageFromServer(message) {
    _isOn = true;
    if (message != "ping") {
      _listeners.forEach((Function callback) {
        callback(message);
      });
    }
  }
}
