import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'socketManager.dart';

GameCommunication game = new GameCommunication();

class GameCommunication {
  static final GameCommunication _game = GameCommunication._internal();

  factory GameCommunication() {
    return _game;
  }

  GameCommunication._internal();

  init() {
    sockets.initCommunication();
    sockets.addListener(_onMessageReceived);
  }

  Function listener = () {};

  _onMessageReceived(serverMessage) {
    try {
      listener(serverMessage);
    } catch (e) {
      print("JSON DECODE ERROR :" + e.toString());
    }
  }

  send(String message) {
    sockets.send(message);
  }

  end() {
    sockets.reset();
  }
}
